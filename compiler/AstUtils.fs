﻿module Dsl.AstUtils

open Dsl.Ast

type GetValue = GetValue with
    static member ($) (GetValue, (Identifier name)) = name

let inline getValue x = GetValue $ x

let getFieldName field =
    match field with
    | Var v ->
        getValue v.id
    | Arr a ->
        getValue a.id
    | Packet p ->
        p.name
