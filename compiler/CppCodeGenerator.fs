﻿module Dsl.CppCodeGenerator

open System.IO
open Dsl.Ast
open Dsl.AstUtils
open Dsl.Writer
open Dsl.Utils.StringBuilder

open CompilerException.CompilerErrors

let writeGuardStart (packetName: string) writer =
    let uppercaseName = packetName.ToUpper() + "_H"
    writer
        >>~ ("#ifndef %s", uppercaseName)
        >>~ ("#define %s", uppercaseName)
        |> newLine

let writeGuardEnd (packetName: string) writer  =
    let uppercaseName = packetName.ToUpper() + "_H"
    writer
        >>~ ("#endif // !%s", uppercaseName)

let writeInclude writer =
    writer
     >>+ "#include <Dsl/DslMemoryBuffer.h>"
     >>+ "#include <Dsl/DslStream.h>"
     |> newLine
     >>+ "#include <cstdint>"
     |> newLine

let writePragmaPackBegin writer =
    writer >>+ "#pragma pack(push, 1)"

let writePragmaPackEnd writer =
    writer >>+ "#pragma pack(pop)"

let writeClassBegin name writer =
    writer >>~ ("class %s_t {", name)

let writeClassEnd writer =
    writer >>+ "};"

let writeConstructor name writer =
    writer
        >>+ "public:"
        |> increaseIndent
        >>~ ("%s_t(dslparser::DslStream *io) {", name)
        |> increaseIndent
        >>+ "_io = io;"
        >>+ "read();"
        |> decreaseIndent
        >>+ "}"
        |> resetIndentLevel
        |> newLine

let writeDestructor name writer =
    writer
        |> increaseIndent
        >>~ ("~%s_t() {", name)
        >>+ "}"
        |> decreaseIndent
        |> newLine

let getCppType =
    function
    | Int8 -> "int8_t"
    | UInt8 -> "uint8_t"
    | Int16 -> "int16_t"
    | UInt16 -> "uint16_t"
    | UInt32 -> "uint32_t"
    | Int32 -> "int32_t"
    | UInt64 -> "uint64_t"
    | Int64 -> "int64_t"
    | Float4 -> "float"
    | Float8 -> "double"

let getIdentifierPublicName id=
    getValue id |> sprintf "%s"

let getIdentifierPrivateName id =
    getValue id |> sprintf "_%s"

let dslVarToField v =
    (getCppType v.dataType, getIdentifierPrivateName v.id) ||> sprintf "%s %s;"

let dslArrayToField id =
    ("std::string", getIdentifierPrivateName id) ||> sprintf "%s %s;"

let fieldToString f =
    match f with
    | Var v ->
        v |> dslVarToField
    | Arr a ->
        a.id |> dslArrayToField
    | _ ->
        f.ToString() |> sprintf "unable to convert field %s" |> codeGeneratorError |> raise

let writeDslFields writer =
    writer >>+ "dslparser::DslStream *_io;"

let writeFields fields writer =
    writer
        >>+ "private:"
        |> increaseIndent
        |> ignore

    fields
        |> Seq.map fieldToString
        |> Seq.iter writer.writeLine

    writer
        |> writeDslFields
        |> resetIndentLevel
        |> newLine

let varToGetterString v =
    (getCppType v.dataType, getIdentifierPublicName v.id, getIdentifierPrivateName v.id) |||> sprintf "%s %s() const { return %s; }"

let arrToGetterString (a: DslArray) =
    ("std::string", getIdentifierPublicName a.id, getIdentifierPrivateName a.id) |||> sprintf "%s %s() const { return %s; }"

let writeGetters fields writer =
    writer
        >>+ "public:"
        |> increaseIndent
        |> ignore

    let fieldToGetter f =
        match f with
        | Var v ->
            v |> varToGetterString
        | Arr a ->
            a |> arrToGetterString
        | _ ->
            f.ToString() |> sprintf "unable to convert field %s to getter" |> codeGeneratorError |> raise

    fields
        |> Seq.map fieldToGetter
        |> Seq.iter writer.writeLine

    writer
        |> resetIndentLevel
        |> newLine

let typeToReadFunc =
    function
    | Int8 -> "_io->read_s1()"
    | UInt8 -> "_io->read_u1()"
    | Int16 -> "_io->read_s2le()"
    | UInt16 -> "_io->read_u2le()"
    | Int32 -> "_io->read_s4le()"
    | UInt32 -> "_io->read_u4le()"
    | Int64 -> "_io->read_s8le"
    | UInt64 -> "_io->read_u8le()"
    | Float4 -> "_io->read_f4le()"
    | Float8 -> "_io->read_f8le()"

let fieldToReadString f =
    let varToString v =
         sprintf "_%s = %s;" (getValue v.id) <| typeToReadFunc v.dataType

    let arrayToReadString (a: DslArray) =
        sprintf "_%s = _io->read_bytes(%i);" (getValue a.id) <| a.size

    match f with
    | Var v ->
        varToString v
    | Arr a ->
        arrayToReadString a
    | _ ->
        f.ToString() |> sprintf "unable to convert field %s to read function" |> codeGeneratorError |> raise


let writeReadFunc packet writer =
    writer
        >>+ "public:"
        |> increaseIndent
        >>+ "void read() {"
        |> increaseIndent
        |> ignore

    packet.fields
        |> Seq.map fieldToReadString
        |> Seq.iter writer.writeLine

    writer
        |> decreaseIndent
        >>+ "}"
        |> resetIndentLevel
        |> newLine

let processPacket writer p =
    writer
        |> writeGuardStart p.name
        |> writeInclude
        |> writeClassBegin p.name
        |> writeConstructor p.name
        |> writeDestructor p.name
        |> writeFields p.fields
        |> writeReadFunc p
        |> writeGetters p.fields
        |> writeClassEnd
        |> writeGuardEnd p.name
        |> ignore

let generate packetList resultFile : unit =
    let sb = new StringBuilder()
    let writer = new Writer(fun line -> sb.Append line |> ignore)

    packetList |> List.iter (processPacket writer)

    use file = File.CreateText(resultFile)
    sb.ToString() |> file.WriteLine
