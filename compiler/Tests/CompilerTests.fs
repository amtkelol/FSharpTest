﻿module Tests.CompilerTests

open FsUnit
open NUnit.Framework
open Dsl
open Dsl.CompilerException

[<Test>]
let ``should raise fieldAlreadyDefined``()=
    let source = "packet foo: 
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        uint16 src_port"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL002 A field named 'src_port' is already defined") typeof<CompilerException>


[<Test>]
let ``should raise reservedKeyword``()=
    let source = "packet foo: 
        uint16 packet_number
        uint16 uint16"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL003 'uint16' is a reserved keyword") typeof<CompilerException>

[<Test>]
let ``should throw nothing``()=
    let source = "packet foo: 
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        uint16 dst_port"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should not' (throw typeof<CompilerException>)

