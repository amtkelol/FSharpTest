﻿module Tests.ParserTests

open Dsl.Ast
open Dsl.Parser
open FParsec
open FsUnit
open NUnit.Framework

type InitMsgUtils() =
    inherit FSharpCustomMessageFormatter()

let runParser parser input =
        match run parser input with
        | Success(v, _, _) ->
            v
        | Failure(msg, _, _) ->
            failwith msg

[<TestFixture>]
type DslParser_Tests ()=
    [<Test>]
    member test.testFramework ()=
        2 + 2 |> should equal 4

    [<Test>]
    member test.``should parse id`` ()=
        runParser parseId "ab" |> should equal (Identifier "ab")

    [<Test>]
    member test.``should parse field`` ()=
        let expected = Var {dataType = UInt16; id = Identifier("name")}
        runParser parseField "uint16 name" |> should equal expected

    [<Test>]
    member test.``should parse packet`` ()=
        let source =
            "packet foo:
                uint16 header
                int8 array[128]
                uint8 version"
        let packet = Dsl.Parser.parse source
        let expected =  [{name = "foo";
                        fields = [Var {dataType = UInt16; id = Identifier "header"};
                                  Arr {dataType = Int8; id = Identifier "array"; size = 128};
                                  Var {dataType = UInt8; id = Identifier "version"}
                        ]}]

        packet |> should Is.EquivalentTo expected

    [<Test>]
    member test.``nested packet test`` ()=
        let source = "packet foo:
            uint16 header
            uint8 version
        packet bar:
            int8 size"
        let packet = Dsl.Parser.parse source
        let expected =  [{name = "foo";
                        fields = [Var {dataType = UInt16; id = Identifier "header"};
                                  Var {dataType = UInt8; id = Identifier "version"}]};
                        {name = "bar";
                        fields = [Var {dataType = Int8; id = Identifier "size"}]}]
        packet |> should Is.EquivalentTo expected

    [<Test>]
    member test.``should parse array`` ()=
        runParser parseField "int8 foo[256]" |> should equal (Arr { dataType = Int8; id = Identifier("foo"); size = 256 })

// NUnit TestCaseSource does not work with .NET Core for some reason
#if !NETCOREAPP2_0

let dataTypeSource =
    [
    "uint8", UInt8
    "uint16", UInt16
    "uint32", UInt32
    "uint64", UInt64
    "int8", Int8
    "int16", Int16
    "int32", Int32
    "int64", Int64
    "float4", Float4
    "float8", Float8
    ] |> Seq.map (fun (a, b) -> TestCaseData(a, b))

[<TestCaseSource("dataTypeSource")>]
let ``should parse data type`` typeString (expectedType: DataType) =
    runParser parseDataType typeString |> should equal expectedType

#endif

[<Test>]
let ``should parse array with space``() =
    let field = "int8 array [128]"
    runParser parseField field |> should equal (Arr { dataType = Int8; id = Identifier("array"); size = 128 })
