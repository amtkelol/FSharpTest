﻿module Dsl.SemanticAnalysis

open AstUtils
open Ast
open CompilerException.CompilerErrors

let findDuplicateVars p =
    let handleDuplicate fields : unit =
        raise (fieldAlreadyDefined (fields |> Seq.head |> getFieldName))

    p.fields
    |> Seq.groupBy id
    |> Seq.map snd
    |> Seq.filter (fun s -> s |> Seq.length > 1)
    |> Seq.iter handleDuplicate

let reservedKeywords = [
    "uint8"; "int8";
    "uint16"; "int16";
    "uint32"; "int32";
    "uint64"; "int64";
    "float4"; "float8"]

let findReservedKeyWords p =
    let fieldNames =
        p.fields
        |> Seq.map getFieldName

    Set.intersect (Set.ofSeq fieldNames) (Set.ofSeq reservedKeywords)
    |> Seq.iter (fun f -> raise (reservedKeyword f))

let analyze packetList =
    packetList |> List.iter (fun p ->
        findDuplicateVars p
        findReservedKeyWords p)
