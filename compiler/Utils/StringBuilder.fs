﻿module Dsl.Utils.StringBuilder

type String = System.String
type StringBuilder = System.Text.StringBuilder

let inline append text (builder : StringBuilder) = builder.Append(sprintf "\"%s\" " text)

let inline forEach items action text (builder: StringBuilder) =
    items
    |> List.iter (fun t -> builder |> (action t text) |> ignore)

    builder

let inline toText (builder : StringBuilder) = builder.ToString()
