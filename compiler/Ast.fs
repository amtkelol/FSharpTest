﻿module Dsl.Ast

type DataType = 
    | Int8
    | Int16
    | Int32
    | Int64
    | UInt8
    | UInt16
    | UInt32
    | UInt64
    | Float4
    | Float8

type Identifier = Identifier of string

type DslArray = {
    dataType: DataType
    id : Identifier
    size: int
}
//type PacketField = {
//    dataType: DataType
//    id: Identifier
//}

type Variable = {
    dataType: DataType
    id: Identifier
}

type PacketField =
    | Var of Variable
    | Packet of Packet
    | Arr of DslArray

and Packet = {
    name: string
    fields: PacketField seq
}
