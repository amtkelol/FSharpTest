﻿module Dsl.Parser

open Dsl.Ast
open FParsec
open Dsl.CompilerException.CompilerErrors

let ws = spaces
let ws1 = spaces1

let parseDataType =
    let int8Str = stringReturn "int8" Int8
    let int16Str = stringReturn "int16" Int16
    let int32Str = stringReturn "int32" Int32
    let int64Str = stringReturn "int64" Int64

    let uint8Str = stringReturn "uint8" UInt8
    let uint16Str = stringReturn "uint16" UInt16
    let uint32Str = stringReturn "uint32" UInt32
    let uint64Str = stringReturn "uint64" UInt64

    let float4Str = stringReturn "float4" Float4
    let float8Str = stringReturn "float8" Float8

    choiceL [int8Str; int16Str; int32Str; int64Str; uint8Str; uint16Str; uint32Str; uint64Str; float4Str; float8Str] "data type"

let isAsciiIdStart c = isAsciiLetter c
let isAsciiIdNext c = isAsciiLetter c || isDigit c || c = '_'
let identOpts = IdentifierOptions(isAsciiIdStart = isAsciiIdStart, isAsciiIdContinue = isAsciiIdNext)

let parseId =
    //let firstChar = asciiLetter
    //let nextChar = anyOf(['a'..'z'] @ ['A'..'Z'] @ ['0'..'9'])

    identifier identOpts |>> Identifier <?> "identifier"

let parseArray =
    let convertToArray ((dType, id), size) =
        Arr { dataType = dType; id = id; size = size }

    parseDataType .>> ws1 .>>. parseId .>> ws .>> pchar('[') .>>. pint32 .>> pchar(']') <?> "array" |>> convertToArray

//let parsePacketStub, parsePacketStubRef = createParserForwardedToRef()

let parseField =
    let parseVar =
        let convertToPacketField (dType, id) =
            Var {dataType = dType; id = id}
        parseDataType .>> ws1 .>>. parseId <?> "variable" |>> convertToPacketField
        
//    parseDataType .>> ws1 .>>. parseId |>> convertToPacketField
//    parseVar <|> parsePacketStub
    attempt parseArray <|> parseVar

let parseSinglePacket =
    let packetKeyword = pstring "packet" <?> "packet"
    let packetName = identifier identOpts <?> "packet name"
    let packetFields = many1 (parseField .>> ws)

    let convertToPacket (pname, pfields) =
        {name = pname; fields = pfields}

    packetKeyword >>. ws1 >>. packetName .>> pchar(':') .>> ws .>>. packetFields |>> convertToPacket <?> "packet"

//parsePacketStubRef := parsePacket

let parsePackets =
    many1 (parseSinglePacket .>> ws) .>> eof

let parse input =
    match run parsePackets input with
    | Success(result, _, _) -> result
    | Failure(msg, _, _) -> raise (parserError msg)

//run parseDataType "uint8"
//run parseId "ab"
//run parseField "uint8 ab"
//run parseSinglePacket "packet foo: uint16 header uint8 version"

//parse "packet foo: uint16 header uint8 version packet bar: int8 size"
//parse "packet foo: uint16 header uint8 version"
