﻿module Dsl.CompilerException

type CompilerException(message : string) =
    inherit System.Exception(message)

module CompilerErrors =
    let create msg = CompilerException msg

    let parserError a = create (sprintf "DSL001 Parser error: %s" a)
    let fieldAlreadyDefined a = create (sprintf "DSL002 A field named '%s' is already defined" a)
    let reservedKeyword a = create (sprintf "DSL003 '%s' is a reserved keyword" a)
    let codeGeneratorError a = create (sprintf "DSL004 Code generator error: %s" a)
