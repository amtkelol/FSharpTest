﻿namespace Dsl

open System

type String = System.String
type WriteFunction = (string -> unit)

type Writer(func: WriteFunction) =
    [<Literal>]
    let indentChar = ' '

    [<Literal>]
    let indentSize = 4

    let mutable indentLevel = 0

    let indentString() =
        match indentLevel with
        | lvl when lvl >= 0 ->
            new String(indentChar, indentSize * lvl)
        | _ ->
            String.Empty

    member this.increaseIndent() =
        indentLevel <- indentLevel + 1

    member this.decreaseIndent() =
        indentLevel <- indentLevel - 1

    member this.resetIndentLevel() =
        indentLevel <- 0

    member this.writeLine line =
        sprintf "%s%s%s" (indentString()) line Environment.NewLine |> func

module Writer =
    let writeLine line (w: Writer) =
        w.writeLine line
        w

    let (>>+) writer line = writeLine line writer

    let writeFormattedLine format (line: string) (w: Writer) =
        sprintf format line |> w.writeLine
        w

    let (>>~) writer (format, line) : Writer = writeFormattedLine format line writer

    let inline newLine (w: Writer) =
        w.writeLine String.Empty
        w

    let inline increaseIndent (w: Writer) =
        w.increaseIndent()
        w

    let inline decreaseIndent (w: Writer) =
        w.decreaseIndent()
        w

    let inline resetIndentLevel (w: Writer) =
        w.resetIndentLevel()
        w
