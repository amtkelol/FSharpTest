﻿module Dsl.Progam

open System.IO
open System.Text
open Dsl
open Dsl.Writer


#if INTERACTIVE
#I "../packages/FParsec/lib/net40-client/"
#r "FParsecCS.dll"
#r "FParsec.dll"
open FParsec
open FParsec.CharParsers

#load "Ast.fs"
open FSI_0003.Dsl

#load "Parser.fs"
open FSI_0005.Dsl
open Dsl.Parser

#load "Utils/StringBuilder.fs"
open FSI_0007.Dsl
open Dsl.Utils.StringBuilder

#load "CppCodeGenerator.fs"
open FSI_0009.Dsl
open Dsl.CppCodeGenerator
#endif

let inline readLines file = File.ReadAllText(file)

let parse source result =
    let input = readLines source
    let ast = Parser.parse input
    CppCodeGenerator.generate ast result

let testParser() =
    let input = "packet foo:
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        uint16 dst_port
        uint64 real_tick
        int8 array_type[4]
        float4 floating_point"
    let ast = Parser.parse input
    SemanticAnalysis.analyze ast
    CppCodeGenerator.generate ast "result.h"

[<EntryPoint>]
let main argv =
    try
        testParser()
    with ex ->
        printfn "Error: %A" ex.Message

    let arglist = argv |> List.ofSeq
    match arglist with
    | source::result::[] ->
        try
            parse source result |> ignore
        with ex ->
            printfn "%A" ex
    | _ ->
        printfn "Usage: sourcefile.foo resultfile"
    0
